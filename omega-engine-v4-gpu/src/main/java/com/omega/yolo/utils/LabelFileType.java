package com.omega.yolo.utils;

/**
 * label file type
 * @author Administrator
 *
 */
public enum LabelFileType {
	
	txt,csv,json
	
}
